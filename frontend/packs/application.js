import "bootstrap";

import "init/index";

require("@rails/ujs").start();
require("@rails/activestorage").start();
require("channels");
